# Releases

# v1.0
https://bitbucket.org/SilicaAndPina/systemappsdisabler/downloads/System.Apps.Disabler.vpk

# SystemAppsDisabler
!!Only for fw3.60 CEX dont run on 3.61+ or D/REX!!    
Fully Disable/Remove Pre-Installed System Apps On Your PSVita.

Written in Lua-Player-Plus

Uses CelesteBlue's "VitaRW For Lua" ^_^ 

# Broken something?
You can restore everything by:                             
Reinstalling your firmware, and then rebuilding the database.                            
